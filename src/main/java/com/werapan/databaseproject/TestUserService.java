package com.werapan.databaseproject;

import com.werapan.databaseproject.model.User;
import com.werapan.databaseproject.service.UserService;

public class TestUserService {

    public static void main(String[] args) {
        UserService userSevice = new UserService();
        User user = userSevice.login("user2", "password");
        if (user != null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("Error");
        }

    }
}
